const request = require('request')
const config = require('./ecosystem.json')

jasmine.DEFAULT_TIMEOUT_INTERVAL = 70000 // 70 seconds timeout

const PORT = 8000
const HOSTS = config.deploy.production.host

test('Test new Proxys', () => {
  let base = 'https://hdn.pt'
  let path = '/'

  let options = {
    url: base + path,
    method: 'GET'
  }

  return requestAll(
    options,
    base,
    body => body.indexOf('HdN Company') !== -1
  ).then(count => {
    expect(count).toBe(HOSTS.length)
  })
})

test('Test new Proxys - PostNL', () => {
  let base = 'http://www.postnl.post'
  let path = '/details'

  let options = {
    url: base + path,
    method: 'POST',
    form: {
      barcodes: 'RU101847033NL'
    }
  }

  return requestAll(
    options,
    base,
    body =>
      body.indexOf(
        'The item has been processed in the country of destination'
      ) !== -1
  ).then(count => {
    expect(count).toBe(HOSTS.length)
  })
})

/*
|--------------------------------------------------------------------------
| Utils
|--------------------------------------------------------------------------
*/
async function requestAll(requestOptions, baseUrl, func) {
  let success = 0

  for (let i = 0; i < HOSTS.length; ++i) {
    requestOptions.proxy = 'http://' + HOSTS[i] + ':' + PORT
    requestOptions.tunnel = false

    if (!requestOptions.headers) {
      requestOptions.headers = {}
    }
    requestOptions.headers['X-Tracker-Url'] = baseUrl

    try {
      let res = await makeRequest(requestOptions)

      if (func(res)) {
        success++
      } else {
        // console.log(HOSTS[i] + ' Failed! Response: ')
      }
    } catch (e) {
      // console.log(HOSTS[i] + ' Failed! Response: ')
    }
  }

  return success
}

function makeRequest(requestOptions) {
  return new Promise((resolve, reject) => {
    request(requestOptions, (error, response, body) => {
      if (error || response.statusCode !== 200) {
        return reject('error or statusCode != 200')
      }

      resolve(body)
    })
  })
}
