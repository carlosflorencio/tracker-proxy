#!/bin/bash

sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

sudo apt-get install -y git nodejs && sudo npm install -g pm2

cd /home/carlos
sudo git clone https://gitlab.com/iamfreee/tracker-proxy.git proxy

cd proxy
sudo npm install
sudo pm2 start index.js --env production --name proxy


