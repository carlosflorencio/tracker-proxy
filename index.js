const httpProxy = require('http-proxy')
const express = require('express')
const bugsnag = require('bugsnag')
bugsnag.register('6b797e41085cd3431e76d3c25cc3fa91')

const app = express()
const proxy = httpProxy.createProxyServer({ proxyTimeout: 60000 })

app.use(bugsnag.requestHandler)

proxy.on('proxyReq', function(proxyReq, req, res, options) {
  proxyReq.removeHeader('X-Tracker-Url')
})

// proxy.on('proxyRes', function(proxyRes, req, res) {
//   let url = req.get('X-Tracker-Url')
//
//   // if (proxyRes.statusCode != 200 && proxyRes.statusCode != 404 && proxyRes.statusCode != 302) {
//   //   bugsnag.notify(
//   //     new Error(req.method + ' ' + url + ' code: ' + proxyRes.statusCode),
//   //     {
//   //       response: {
//   //         path: url,
//   //         statusCode: proxyRes.statusCode,
//   //         headers: proxyRes.headers
//   //       }
//   //     }
//   //   )
//   // }
// })

app.get('/ping', function(req, res) {
  res.send('pong4')
})

app.use(function(req, res) {
  let url = req.get('X-Tracker-Url')

  proxy.web(req, res, { target: url, changeOrigin: true })
})

app.use(bugsnag.errorHandler)

app.listen(8000, function() {
  console.log('Proxy app listening on port 8000!')
})
